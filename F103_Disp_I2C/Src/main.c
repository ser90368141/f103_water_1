/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 ** This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * COPYRIGHT(c) 2019 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "LCD_I2C.h"
#include "string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */


	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_AFIO);
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

	NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

	/* System interrupt init*/

	/**NOJTAG: JTAG-DP Disabled and SW-DP Enabled
	 */
	LL_GPIO_AF_Remap_SWJ_NOJTAG();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	/* USER CODE BEGIN 2 */
	UART1_Init();

	char str_lcd[25] = {0};
	char str1[16] = {0};
	uint8_t packet_receive[25] = {0};

	DWT_Init();
	LL_mDelay(500);
	LCD_I2C_Init();
	LL_mDelay(100);
	LCD_Clear();

	sprintf(str_lcd, "Hello!");
	LCD_SetPos(0,0);
	LCD_Send_Str(str_lcd);

	RingBuffer_Clear(&UART1_RX_RingBuffer);

	//------------------------------

	uint8_t tmp1 = 0xFF;
	uint8_t tmp2 = 0xFF;

	uint8_t command = 0;
	uint8_t len = 0;
	uint8_t com_len_CRC = 0;
	uint8_t com_len_CRC_calc = 0;
	uint8_t pack_CRC = 0;
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

		if (RingBuffer_Size(&UART1_RX_RingBuffer)) // Если буфер приёма не пуст
		{
			tmp1 = RingBuffer_Get(&UART1_RX_RingBuffer);
			if (tmp1 == 0x00) // Если пришёл IDLE
			{
				tmp2 = tmp1;
			}
			else if (tmp1 == 0xFF && tmp2 == 0x00) // Определяем приход START
			{
				// Готовимся к приёму нового пакета
				tmp2 = 0xFF; tmp1 = 0xFF;
				// Здесь обрабатываем приход пакета

				while (!RingBuffer_Size(&UART1_RX_RingBuffer));
				// Должна прийти команда
				command = RingBuffer_Get(&UART1_RX_RingBuffer);

				while (!RingBuffer_Size(&UART1_RX_RingBuffer));

				len = RingBuffer_Get(&UART1_RX_RingBuffer);

				while (!RingBuffer_Size(&UART1_RX_RingBuffer));

				com_len_CRC = RingBuffer_Get(&UART1_RX_RingBuffer);

				com_len_CRC_calc = command + len;
				com_len_CRC_calc = ~com_len_CRC_calc;
				com_len_CRC_calc += 1;

				if (com_len_CRC_calc == com_len_CRC) // Если CRC команды и пакета правильный
				{
					uint8_t sum = 0;
					for (uint8_t i = 0; i < len; i++) // Принимаем весь пакет
					{
						while (!RingBuffer_Size(&UART1_RX_RingBuffer));
						tmp1 = RingBuffer_Get(&UART1_RX_RingBuffer);

						packet_receive[i] = tmp1;
						sum += tmp1;
					}
					packet_receive[len] = 0;

					while (!RingBuffer_Size(&UART1_RX_RingBuffer));

					pack_CRC = RingBuffer_Get(&UART1_RX_RingBuffer);

					sum = ~sum;
					sum += 1;

					if (sum == pack_CRC) // Проверяем контрольную сумму пакета
					{
						while (!RingBuffer_Size(&UART1_RX_RingBuffer));
						if (RingBuffer_Get(&UART1_RX_RingBuffer) == 0xFF)
						{
							while (!RingBuffer_Size(&UART1_RX_RingBuffer));
							if (RingBuffer_Get(&UART1_RX_RingBuffer) == 0x00) // Пришло условие стоп
							{
								uint16_t dad_value = 0;
								uint8_t parcent = 0;
								uint8_t duty = 0;
								uint16_t rpm = 0;
								uint8_t max_duty = 0;

								switch (command) {
								case 1: // Если пришла команда вывода аски в строку 1
									LCD_SetPos(0, 0);

									// Заполняем остатки строки пробелами
									for (uint8_t i = len; i < 17; i++)
									{
										packet_receive[i] = ' ';
									}
									packet_receive[17] = 0;
									LCD_Send_Str((char*)packet_receive);
									break;
								case 2: // Если пришла команда вывода аски в строку 2
									LCD_SetPos(0, 1);

									// Заполняем остатки строки пробелами
									for (uint8_t i = len; i < 17; i++)
									{
										packet_receive[i] = ' ';
									}
									packet_receive[17] = 0;

									LCD_Send_Str((char*)packet_receive);
									break;
								case 5: // Пришли данные для 1 режима
									if (len != 3) break; // Если неправильная длинна пакета

									// Извлекаем данные из пакета
									dad_value = ((uint16_t)packet_receive[0]) << 8;
									dad_value |= (uint16_t)packet_receive[1];
									parcent = packet_receive[2];

									int dad_value_int = ((int)dad_value) - 100;

									// Формируем строку вывода давления
									strcpy(str_lcd, "Pres: ");

									if (dad_value_int < 0)
									{
										str_lcd[6] = '-';
										dad_value_int *= -1;
									}
									else str_lcd[6] = ' ';

									uint8_t i3 = dad_value_int % 10;
									dad_value_int /= 10;

									uint8_t i2 = dad_value_int % 10;
									dad_value_int /= 10;

									uint8_t i1 = dad_value_int % 10;

									itoa(i1, str_lcd + 7, 10);
									str_lcd[8] = ',';
									itoa(i2, str_lcd + 9, 10);
									itoa(i3, str_lcd + 10, 10);

									strcat(str_lcd, " bar ");

									// Выводим стоку на экран
									LCD_SetPos(0, 0);
									LCD_Send_Str(str_lcd);


									// Формируем строку вывода степени впрыска
									strcpy(str_lcd, "Inject: ");

									itoa(parcent, str1, 10);
									for (uint8_t i = strlen(str1); i < 3; i++)
									{
										strcat(str_lcd, " ");
									}
									strcat(str_lcd, str1);

									strcat(str_lcd, "%");

									// Добиваем строку нулями
									while (strlen(str_lcd) < 16)
									{
										strcat(str_lcd, " ");
									}

									// Выводим стоку на экран
									LCD_SetPos(0, 1);
									LCD_Send_Str(str_lcd);

									break;
								case 6:
									if (len != 4) break;

									duty = packet_receive[0];
									parcent = packet_receive[1];

									rpm = ((uint16_t)packet_receive[2]) << 8;
									rpm |= (uint16_t)packet_receive[3];

									// Формируем строку вывода загрузки и оборотов
									strcpy(str_lcd, "DT");
									itoa(duty, str1, 10);
									for (uint8_t i = strlen(str1); i < 3; i++)
									{
										strcat(str_lcd, " ");
									}
									strcat(str_lcd, str1);

									strcat(str_lcd, "% RPM ");
									itoa(rpm, str1, 10);
									for (uint8_t i = strlen(str1); i < 4; i++)
									{
										strcat(str_lcd, " ");
									}
									strcat(str_lcd, str1);

									// Добиваем строку нулями
									while (strlen(str_lcd) < 16)
									{
										strcat(str_lcd, " ");
									}

									// Выводим стоку на экран
									LCD_SetPos(0, 0);
									LCD_Send_Str(str_lcd);


									// Формируем строку вывода степени впрыска
									strcpy(str_lcd, "Inject: ");

									itoa(parcent, str1, 10);
									for (uint8_t i = strlen(str1); i < 3; i++)
									{
										strcat(str_lcd, " ");
									}
									strcat(str_lcd, str1);

									strcat(str_lcd, "%");

									// Добиваем строку нулями
									while (strlen(str_lcd) < 16)
									{
										strcat(str_lcd, " ");
									}
									// Выводим стоку на экран
									LCD_SetPos(0, 1);
									LCD_Send_Str(str_lcd);

									break;

								case 7:
									if (len != 4) break;

									duty = packet_receive[0];
									max_duty = packet_receive[1];

									rpm = ((uint16_t)packet_receive[2]) << 8;
									rpm |= (uint16_t)packet_receive[3];

									// Формируем строку вывода загрузки и оборотов
									strcpy(str_lcd, "DT");
									itoa(duty, str1, 10);
									for (uint8_t i = strlen(str1); i < 3; i++)
									{
										strcat(str_lcd, " ");
									}
									strcat(str_lcd, str1);

									strcat(str_lcd, "% RPM ");
									itoa(rpm, str1, 10);
									for (uint8_t i = strlen(str1); i < 4; i++)
									{
										strcat(str_lcd, " ");
									}
									strcat(str_lcd, str1);

									// Добиваем строку нулями
									while (strlen(str_lcd) < 16)
									{
										strcat(str_lcd, " ");
									}

									// Выводим стоку на экран
									LCD_SetPos(0, 0);
									LCD_Send_Str(str_lcd);

									// Формируем строку вывода максимальной загрузки

									strcpy(str_lcd, "Max Duty ");
									itoa(max_duty, str1, 10);

									strcat(str_lcd, str1);
									strcat(str_lcd, "%");

									// Добиваем строку нулями
									while (strlen(str_lcd) < 16)
									{
										strcat(str_lcd, " ");
									}
									// Выводим стоку на экран
									LCD_SetPos(0, 1);
									LCD_Send_Str(str_lcd);

									break;

								case 8:
									if (len != 1) break;

									if (packet_receive[0] != 0x55) break;

									strcpy(str_lcd, "FUEL END!");

									// Добиваем строку нулями
									while (strlen(str_lcd) < 16)
									{
										strcat(str_lcd, " ");
									}
									// Выводим стоку на экран
									LCD_SetPos(0, 1);
									LCD_Send_Str(str_lcd);
									LCD_SetPos(0, 0);
									LCD_Send_Str(str_lcd);

									break;
								default:
									break;
								}
							}
						}
					}
				}

			}
		}

	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);

	if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_2)
	{
		Error_Handler();
	}
	LL_RCC_HSE_Enable();

	/* Wait till HSE is ready */
	while(LL_RCC_HSE_IsReady() != 1)
	{

	}
	LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE_DIV_1, LL_RCC_PLL_MUL_9);
	LL_RCC_PLL_Enable();

	/* Wait till PLL is ready */
	while(LL_RCC_PLL_IsReady() != 1)
	{

	}
	LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
	LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
	LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
	LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

	/* Wait till System clock is ready */
	while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
	{

	}
	LL_Init1msTick(72000000);
	LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
	LL_SetSystemCoreClock(72000000);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{ 
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
