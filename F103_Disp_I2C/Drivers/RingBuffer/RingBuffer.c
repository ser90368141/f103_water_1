/*
 * RingBuffer.c
 *
 *  Created on: 13 янв. 2019 г.
 *      Author: sergey
 */
#include "RingBuffer.h"

void RingBuffer_Init(RingBuffer_struct * RingBuffer_in)
{
	for (uint16_t i = 0; i < 256; i++)
	{
		RingBuffer_in->buffer[i] = 0;
	}
	RingBuffer_in->index_in = 0;
	RingBuffer_in->index_out = 0;
}

void RingBuffer_Put(RingBuffer_struct * RingBuffer_in, uint8_t data_in)
{
	RingBuffer_in->buffer[RingBuffer_in->index_in++] = data_in;
	//	RingBuffer_in->indexIn &= BUF_MASK;
}

uint8_t RingBuffer_Get(RingBuffer_struct * RingBuffer_in)
{
	if (RingBuffer_Size(RingBuffer_in) <= 0) return 0;
	return RingBuffer_in->buffer[RingBuffer_in->index_out++];
}

uint8_t RingBuffer_Size(RingBuffer_struct* RingBuffer_in)
{
	if (RingBuffer_in->index_in >= RingBuffer_in->index_out)
		return (RingBuffer_in->index_in - RingBuffer_in->index_out);
	else
		return ((256 - RingBuffer_in->index_out) + RingBuffer_in->index_in);
}

void RingBuffer_Clear(RingBuffer_struct* RingBuffer_in)
{
	RingBuffer_in->index_out = RingBuffer_in->index_in;
}
