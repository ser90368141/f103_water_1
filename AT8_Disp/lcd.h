﻿#ifndef LCD_H_
#define LCD_H_

#include "main.h"
/*
Подключаем так:
RS  	PD2
E 	 	PD3
D4  	PD4
D5  	PD5
D6  	PD6
D7  	PD7
LED  	PC3
*/

#define LCD_PORT	PORTD
#define LCD_DDR		DDRD
#define LED_PORT	PORTC
#define LED_DDR		DDRC

#define RS			PD2
#define E			PD3
#define D4			PD4
#define D5			PD5
#define D6			PD6
#define D7			PD7
#define LED			PC3

//----------------------------
static inline void e1()
{
	LCD_PORT |= 1<<E;
}
static inline void e0()
{
	LCD_PORT &= ~(1<<E);
}
static inline void rs1()
{
	LCD_PORT |= 1<<RS;
}
static inline void rs0()
{
	LCD_PORT &= ~(1<<RS);
}
static inline void LCD_ledON()
{
	LED_PORT |= 1<<LED;
}
static inline void LCD_ledOFF()
{
	LED_PORT &= ~(1<<LED);
}
//-----------------------------
void LCD_init(void);
void LCD_setpos(uint8_t x, uint8_t y);
void LCD_sendchar(uint8_t c);
void LCD_sendstr (char str1[]);
void LCD_clear();
//----------------------------------------

#endif /* LCD_H_ */
