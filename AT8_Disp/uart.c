/*
 * uart.c
 *
 * Created: 03.02.2017 23:39:42
 *  Author: Sergey
 */ 
#include "uart.h"

//----------------------------------------

RingBuffer_struct RingBuffer;

ISR(USART_RXC_vect)
{
	cli();
	uint8_t data_in;
	data_in = UDR;
	RingBuffer_Put(&RingBuffer, data_in);
	sei();
}

void UART_Init(uint16_t speed)
{
	PORTD |= (1 << PD0) | (1 << PD1);
	DDRD |= (1 << PD0) | (1 << PD1);

	UBRRH = 0;
	if (speed == 9600)
	{
		UBRRL = 51;
	}
	else if (speed == 4800)
	{
		UBRRL = 103;
	}
	else if (speed == 2400)
	{
		UBRRL = 207;
	}
	else
	{
		UBRRL = 51;
	}
	UCSRB = (1 << TXEN) | (1 << RXEN); // Enable RX and TX
	UCSRB |= (1 << RXCIE); // Enable RX interrupt

	UCSRC = (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0);

	// Init RingBuffer
	RingBuffer_Init(&RingBuffer);
}

void UART_Transmit(uint8_t data)
{
	loop_until_bit_is_set(UCSRA, UDRE);    
	UDR = data;
}

void UART_Transmit_str(char str[])
{
	for (uint16_t i = 0; str[i] != '\0'; i++)
	{
		UART_Transmit(str[i]);
	}
}
