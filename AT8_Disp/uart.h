/*
 * uart.h
 *
 * Created: 03.02.2017 23:39:30
 *  Author: Sergey
 */ 
#ifndef UART_H_
#define UART_H_

#include "main.h"

void UART_Init(uint16_t speed);
void UART_Transmit(uint8_t data);
void UART_Transmit_str(char str[]);

#endif /* UART_H_ */
