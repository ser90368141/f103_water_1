﻿#include "lcd.h"


//----------------------------------------
void LCD_port_init()
{
	LCD_PORT &= ~(1 << RS | 1 << E | 1 << D4 | 1 << D5 | 1 << D6 | 1 << D7);
	LCD_DDR |= 1 << RS | 1 << E | 1 << D4 | 1 << D5 | 1 << D6 | 1 << D7;

	LED_DDR = 1<<LED;
	LED_PORT &= ~(1<<LED);
}
//----------------------------------------
void sendhalfbyte(uint8_t c)
{
	c &= 0b00001111;
	c <<= 4;
	e1(); //включаем линию Е
	_delay_us(50);
	LCD_PORT &= ~(1 << D7| 1 << D6 | 1 << D5 | 1 << D4);
	LCD_PORT |= c;
	e0(); //выключаем линию Е
	_delay_us(50);
}
//----------------------------------------
void sendbyte(uint8_t c, uint8_t mode)
{
	if (mode==0) rs0();
	else         rs1();
	uint8_t hc = 0;
	hc = c >> 4;
	sendhalfbyte(hc);
	sendhalfbyte(c);
}
//----------------------------------------
void LCD_sendchar(uint8_t c)
{
	sendbyte(c,1);
}
//----------------------------------------
void LCD_setpos(uint8_t x, uint8_t y)
{
	char adress;
	adress=(0x40*y+x)|0b10000000;
	sendbyte(adress, 0);
}
//----------------------------------------
void LCD_init(void)
{
	LCD_port_init();

	_delay_ms(15); //Ждем 15 мс (стр 45)
	sendhalfbyte(0b00000011);
	_delay_ms(4);
	sendhalfbyte(0b00000011);
	_delay_us(100);
	sendhalfbyte(0b00000011);
	_delay_ms(1);
	sendhalfbyte(0b00000010);
	_delay_ms(1);
	sendbyte(0b00101000, 0); //4бит-режим (DL=0) и 2 линии (N=1)
	_delay_ms(1);
	sendbyte(0b00001100, 0); //включаем изображение на дисплее (D=1), курсоры никакие не включаем (C=0, B=0)
	_delay_ms(1);
	sendbyte(0b00000110, 0); //курсор (хоть он у нас и невидимый) будет двигаться влево
	_delay_ms(1);
}
//----------------------------------------
void LCD_clear()
{
	sendbyte(0b00000001, 0);
	_delay_us(1500);
}
//----------------------------------------
void LCD_sendstr (char str1[])
{
	for(uint8_t n = 0; str1[n] != '\0'; n++)
	{
		LCD_sendchar(str1[n]);
	}
}
//----------------------------------------
