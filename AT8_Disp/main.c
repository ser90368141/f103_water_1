/*
 * main.c
 *
 *  Created on: 11 мар. 2019 г.
 *      Author: sergey
 */

#include "main.h"

extern RingBuffer_struct RingBuffer;


int main(void)
{
	LCD_init();
	LCD_ledON();

	LCD_clear();

	LCD_setpos(0, 0);
	LCD_sendstr("Water By Nester");
	LCD_setpos(0, 1);
	LCD_sendstr("2019");

	UART_Init(9600);

	sei();

	while(1)
	{
		if (RingBuffer_Size(&RingBuffer))
		{
			uint8_t data_in = RingBuffer_Get(&RingBuffer);
			if (data_in == '1')
			{
				LCD_ledON();
				UART_Transmit_str("1\r\n");
			}
			else
			{
				LCD_ledOFF();
				UART_Transmit_str("0\r\n");
			}
		}
	}
}

