/*
 * ringbuffer.h
 *
 *  Created on: 11 мар. 2019 г.
 *      Author: sergey
 */

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

#include "main.h"

typedef struct
{
	uint8_t buffer[256];
	uint8_t index_in;
	uint8_t index_out;
} RingBuffer_struct;

void RingBuffer_Init(RingBuffer_struct* RingBuffer_in);
void RingBuffer_Put(RingBuffer_struct* RingBuffer_in, uint8_t data_in);
uint8_t RingBuffer_Get(RingBuffer_struct* RingBuffer_in);
uint8_t RingBuffer_Size(RingBuffer_struct* RingBuffer_in);
void RingBuffer_Clear(RingBuffer_struct* RingBuffer_in);

#endif /* RINGBUFFER_H_ */
