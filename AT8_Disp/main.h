/*
 * main.h
 *
 *  Created on: 11 мар. 2019 г.
 *      Author: sergey
 */

#ifndef MAIN_H_
#define MAIN_H_

#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include "string.h"

#include "lcd.h"
#include "uart.h"
#include "ringbuffer.h"

#endif /* MAIN_H_ */
