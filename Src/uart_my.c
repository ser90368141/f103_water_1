/*
 * uart.c
 *
 *  Created on: 9 мая 2018 г.
 *      Author: sergey
 */

#include <uart_my.h>
#include "stdlib.h"
#include "string.h"

uint8_t Uart1_RxBuf[256] = {0};
uint8_t Uart1_RxBuxCnt = 0;

uint8_t Uart2_RxBuf[256] = {0};
uint8_t Uart2_RxBuxCnt = 0;

void UART1_Port_Init(void)
{
	// TX - PA9, RX - PA10
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN | RCC_APB2ENR_IOPAEN; // Enable clocking AFIO and PortA
	// Init PA9 as Alternative Push-Pull
	GPIOA->CRH |= GPIO_CRH_MODE9; // Speed Max
	GPIOA->CRH &= ~GPIO_CRH_CNF9; //
	GPIOA->CRH |= GPIO_CRH_CNF9_1; // Alternative Push-Pull
	// Init PA10 as Input Floating
	GPIOA->CRH &= ~GPIO_CRH_MODE10; // Mode = 00
	GPIOA->CRH &= ~GPIO_CRH_CNF10;
	GPIOA->CRH |= GPIO_CRH_CNF10_0; // Input floating
}

void UART2_Port_Init(void)
{
	// TX - PA2, RX - PA3
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN | RCC_APB2ENR_IOPAEN; // Enable clocking AFIO and PortA
	// Init PA2 as Alternative Push-Pull
	GPIOA->CRL |= GPIO_CRL_MODE2; // Speed Max
	GPIOA->CRL &= ~GPIO_CRL_CNF2; //
	GPIOA->CRL |= GPIO_CRL_CNF2_1; // Alternative Push-Pull
	// Init PA3 as Input Floating
	GPIOA->CRL &= ~GPIO_CRL_MODE3; // Mode = 00
	GPIOA->CRL &= ~GPIO_CRL_CNF3;
	GPIOA->CRL |= GPIO_CRL_CNF3_0; // Input floating
}

uint8_t UART1_Init(uint32_t baudrate_in)
{
	// Init UART1 Port
	UART1_Port_Init();
	// Init UART1
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN; // Enable clocking UART1
	// Init BaudRate
	uint32_t brr_temp = 0;
	brr_temp = UART1_BusFreq + (baudrate_in >> 1);
	brr_temp /= baudrate_in;
	USART1->BRR = brr_temp;
	// Uart Init
	USART1->CR1 |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RXNEIE;

	NVIC_EnableIRQ(USART1_IRQn);
	NVIC_SetPriority(USART1_IRQn, 0);

	return 1;
}

uint8_t UART2_Init(uint32_t baudrate_in)
{
	// Init UART1 Port
	UART2_Port_Init();
	// Init UART1
	RCC->APB1ENR |= RCC_APB1ENR_USART2EN; // Enable clocking UART2
	// Init BaudRate
	uint32_t brr_temp = 0;
	brr_temp = UART2_BusFreq + (baudrate_in >> 1);
	brr_temp /= baudrate_in;
	USART2->BRR = brr_temp;
	// Uart Init
	USART2->CR1 |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RXNEIE;

	NVIC_EnableIRQ(USART2_IRQn);
	NVIC_SetPriority(USART2_IRQn, 0);

	return 1;
}

void UART1_TransmitEnable(void)
{
	USART1->CR1 |= USART_CR1_TE;
}

void UART2_TransmitEnable(void)
{
	USART2->CR1 |= USART_CR1_TE;
}

void UART1_TransmitDisable(void)
{
	USART1->CR1 &= ~USART_CR1_TE;
}

void UART2_TransmitDisable(void)
{
	USART2->CR1 &= ~USART_CR1_TE;
}

void UART1_ReciveEnable(void)
{
	USART1->CR1 |= USART_CR1_RE;
}

void UART2_ReciveEnable(void)
{
	USART2->CR1 |= USART_CR1_RE;
}

void UART1_ReciveDisable(void)
{
	USART1->CR1 &= ~USART_CR1_RE;
}

void UART2_ReciveDisable(void)
{
	USART2->CR1 &= ~USART_CR1_RE;
}

void UART1_SendByte(uint8_t byte_in)
{
	while (!(USART1->SR & USART_SR_TXE)); // Wait flag transmit register empty
	USART1->DR = byte_in; // Send byte
}

void UART2_SendByte(uint8_t byte_in)
{
	while (!(USART2->SR & USART_SR_TXE)); // Wait flag transmit register empty
	USART2->DR = byte_in; // Send byte
}

void UART1_SendBuf(uint8_t* Buf_in, uint16_t Buf_len_in)
{
	for(uint16_t counter = 0; counter < Buf_len_in; counter++) UART1_SendByte(Buf_in[counter]);
	while (!(USART1->SR & USART_SR_TC)); // Wait flag transmit complete
}

void UART2_SendBuf(uint8_t* Buf_in, uint16_t Buf_len_in)
{
	for(uint16_t counter = 0; counter < Buf_len_in; counter++) UART2_SendByte(Buf_in[counter]);
	while (!(USART2->SR & USART_SR_TC)); // Wait flag transmit complete
}

void USART1_IRQHandler(void)
{
	if (USART1->SR & USART_SR_RXNE) // Recive DR not empty
	{
		USART1->SR &= ~USART_SR_RXNE; // Reset flag
		Uart1_RxBuf[Uart1_RxBuxCnt] = USART1->DR;
		Uart1_RxBuxCnt++;
	}
}

void USART2_IRQHandler(void)
{
	if (USART2->SR & USART_SR_RXNE) // Recive DR not empty
	{
		USART2->SR &= ~USART_SR_RXNE; // Reset flag
		Uart2_RxBuf[Uart2_RxBuxCnt] = USART2->DR;
		Uart2_RxBuxCnt++;
	}
}

void UART1_SendDAD(int DAD_value)
{
	char str1[30] = {0};
	char str2[30] = {0};

	itoa(DAD_value, str1, 10);
	strcpy(str2, "Pres: ");

	if (DAD_value < 0)//if (strlen(str1) == 4)//
	{
		str2[6] = '-';
		DAD_value *= -1;
	}
	else str2[6] = ' ';

	uint8_t i3 = DAD_value % 10;
	DAD_value /= 10;

	uint8_t i2 = DAD_value % 10;
	DAD_value /= 10;

	uint8_t i1 = DAD_value % 10;

	itoa(i1, str2 + 7, 10);
	str2[8] = ',';
	itoa(i2, str2 + 9, 10);
	itoa(i3, str2 + 10, 10);

	strcat(str2, " bar");


	strcpy(str1, "strt");

	str1[4] = 1;
	str1[5] = strlen(str2);
	str1[6] = 0;

	strcat(str1, str2);

	strcat(str1, "endd");

	UART1_SendBuf((uint8_t*)str1, strlen(str1));
}

void UART1_SendPercent(uint8_t pers_in)
{
	char str1[30] = {0};
	char str2[30] = {0};

	strcpy(str2, "Inject: ");

	itoa(pers_in, str1, 10);

	strcat(str2, str1);

	strcat(str2, "%");


	strcpy(str1, "strt");
	str1[4] = 2;
	str1[5] = strlen(str2);
	str1[6] = 0;
	strcat(str1, str2);
	strcat(str1, "endd");
	UART1_SendBuf((uint8_t*)str1, strlen(str1));
}

void UART1_SendDutyRPM(uint16_t duty, uint16_t rpm_value)
{
	char str1[30] = {0};
	char str2[30] = {0};

	strcpy(str2, "DT");
	itoa(duty, str1, 10);
	for (uint8_t i = strlen(str1); i < 3; i++)
	{
		strcat(str2, " ");
	}
	strcat(str2, str1);
	strcat(str2, "% RPM ");
	itoa(rpm_value, str1, 10);
	for (uint8_t i = strlen(str1); i < 4; i++)
	{
		strcat(str2, " ");
	}
	strcat(str2, str1);

	strcpy(str1, "strt");
	str1[4] = 1;
	str1[5] = strlen(str2);
	str1[6] = 0;
	strcat(str1, str2);
	strcat(str1, "endd");
	UART1_SendBuf((uint8_t*)str1, strlen(str1));
}

void UART1_SendMaxDuty(uint16_t duty)
{
	char str1[30] = {0};
	char str2[30] = {0};

	strcpy(str2, "Max Duty ");
	itoa(duty, str1, 10);

	strcat(str2, str1);
	strcat(str2, "%");

	strcpy(str1, "strt");
	str1[4] = 2;
	str1[5] = strlen(str2);
	str1[6] = 0;
	strcat(str1, str2);
	strcat(str1, "endd");
	UART1_SendBuf((uint8_t*)str1, strlen(str1));
}
