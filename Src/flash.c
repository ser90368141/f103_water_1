/*
 * flash.c
 *
 *  Created on: 17 мая 2018 г.
 *      Author: sergey
 */

#include "flash.h"

void FLASH_Unlock(void)
{
	FLASH->KEYR = FLASH_KEY1;
	FLASH->KEYR = FLASH_KEY2;
}

void FLASH_Lock(void)
{
	FLASH->CR |= FLASH_CR_LOCK;
}

uint8_t flash_ready(void)
{
	return !(FLASH->SR & FLASH_SR_BSY);
}

uint8_t check_EOP(void)
{
	if(FLASH->SR & FLASH_SR_EOP)
	{
		FLASH->SR &= ~FLASH_SR_EOP; //!!!!!!!!!!!!
		return 1;
	}
	return 0;
}

uint8_t FLASH_Erase_Page(uint32_t address)
{
	while(!flash_ready()); //Ожидаем готовности флеша к записи

	FLASH->CR |= FLASH_CR_PER; //Устанавливаем бит стирания одной страницы
	FLASH->AR = address; // Задаем её адрес
	FLASH->CR |= FLASH_CR_STRT; // Запускаем стирание
	while(!flash_ready());  //Ждем пока страница сотрется
	FLASH->CR &= ~FLASH_CR_PER; //Сбрасываем бит стирания одной страницы

	return check_EOP();//операция завершена, очищаем флаг
}

uint8_t FLASH_Program_Word(uint32_t address, uint32_t data)
{
	uint16_t data_tmp = 0;
	if (address > FLASH_END) return 0; // Если привышаем размеры флеша
	while(!flash_ready()); //Ожидаем готовности флеша к записи

	FLASH->CR |= FLASH_CR_PG; //Разрешаем программирование флеша
	data_tmp = (uint16_t)data; //Берем младшие 2 байта
	*(__IO uint16_t*)address = data_tmp;
	while(!flash_ready());//Ждем завершения операции
	if(!check_EOP())return 0;

	address += 2;//Прибавляем к адресу два байта
	data_tmp = data >> 16;//Сдвигаем данные
	*(__IO uint16_t*)address = data_tmp; //Пишем старшие 2 байта
	while(!flash_ready());//Ждем завершения операции
	FLASH->CR &= ~(FLASH_CR_PG); //Запрещаем программирование флеша

	return check_EOP();
}


//data= (*(__IO uint32_t*) address)
