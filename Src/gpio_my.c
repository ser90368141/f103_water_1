/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <gpio_my.h>
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */
void GPIO_LED_STOCK_Init(void) // Configure LED_STOCK
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;

	GPIOC->CRH &= ~(GPIO_CRH_CNF13_1 | GPIO_CRH_CNF13_0); // Push-pull out
	GPIOC->CRH |= (GPIO_CRH_MODE13_1 | GPIO_CRH_MODE13_0); // Speed 50MHz

	GPIOC->BSRR |= GPIO_BSRR_BS13; // LED off
}

void GPIO_LED_STOCK_Tog()
{
	if (READ_BIT(GPIOC->IDR, GPIO_IDR_IDR13))
	{
		LED_STOCK_ON();
	}
	else
	{
		LED_STOCK_OFF();
	}
}

void GPIO_Init(void)
{
	/* Init PWM Out <-> PA15 Outpup Open-Drain */
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;

	GPIOA->CRH |= GPIO_CRH_MODE15; // Full Speed
	GPIOA->CRH &= ~GPIO_CRH_CNF15;
	GPIOA->CRH |= GPIO_CRH_CNF15_0; // Out Open-Drain

	GPIOA->BSRR |= GPIO_BSRR_BR15; // PWM off

	/* Init PUMP_OUT <-> PA11 output Open-Drain */
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;

	GPIOA->CRH |= GPIO_CRH_MODE11; // Full Speed
	GPIOA->CRH &= ~GPIO_CRH_CNF11;
	GPIOA->CRH |= GPIO_CRH_CNF11_0; // Out Open-Drain

	GPIOA->BSRR |= GPIO_BSRR_BR11; // PWM off

	/* Init FUEL_STATE <-> PA12 Input Pull-Up */
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;

	GPIOA->CRH &= ~GPIO_CRH_MODE12; // Speed Off
	GPIOA->CRH &= ~GPIO_CRH_CNF12;
	GPIOA->CRH |= GPIO_CRH_CNF12_1; // Input No analog

	GPIOA->ODR |= GPIO_ODR_ODR12; // Pull-Up

	/* Init TRG <-> PB6 Input Floating */

	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;

	GPIOB->CRL &= ~GPIO_CRL_MODE6;
	GPIOB->CRL &= ~GPIO_CRL_CNF6;
	GPIOB->CRL |= GPIO_CRL_CNF6_0;
}
/* USER CODE END 1 */

///** Configure pins as
//        * Analog
//        * Input
//        * Output
//        * EVENT_OUT
//        * EXTI
//*/
//void MX_GPIO_Init(void)
//{
//
//  /* GPIO Ports Clock Enable */
//  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOD);
//  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);
//
//}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
