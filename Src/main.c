/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 ** This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * COPYRIGHT(c) 2019 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "string.h"

#include "firmware.h"
#include "disp_module.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint8_t current_map = 0;
uint16_t current_duty = 0;
uint16_t duty_coef = 0;
uint16_t dad_value = 0;
uint16_t RPM_value = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void PC_UART_Poll(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
char str3[25] = {0};
/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */


	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_AFIO);
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

	NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

	/* System interrupt init*/

	/**NOJTAG: JTAG-DP Disabled and SW-DP Enabled
	 */
	LL_GPIO_AF_Remap_SWJ_NOJTAG();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	/* USER CODE BEGIN 2 */
	PUMP_OFF();
	LED_STOCK_OFF();

	UART1_Init();
	UART2_Init();

	//	FirmWare_SetStockValue();
	//	FirmFare_WriteCurValue();
	FirmWare_Init();

	WORK_Timer_Init(0);

	LL_mDelay(500);

	// Выводим приветствие
	sprintf(str3, "Water by Nester");
	disp_module_send_1_string(str3);
	disp_module_send_1_string(str3);
	LL_mDelay(100);

	if (FirmWare_SRAM.type == 1) // Если режим работы по ДАД
	{
		sprintf(str3, "Type: MAP");
		disp_module_send_2_string(str3);
		disp_module_send_2_string(str3);
		LL_mDelay(1000);

		// Запускаем ацп
		ADC_Init();

		// Запускаем ШИМ с указанной частотой
		PWM_Timer_Init(FirmWare_SRAM.freq);
		PWM_Timer_SetComp(0);
		PWM_Timer_Start();

		// Таймер захвата останавливаем
		Duty_Timer_Init();
		Duty_Timer_Stop();

		// На всякий случай останавливаем насос
		PUMP_OFF();

		LL_mDelay(1000);
	}
	else if (FirmWare_SRAM.type == 2) // Если режим работы по циклической загрузке
	{
		sprintf(str3, "Type: DUTY");
		disp_module_send_2_string(str3);
		disp_module_send_2_string(str3);
		LL_mDelay(1000);

		// Останавливаем ацп
		ADC_DeInit();

		// Запускаем ШИМ с указанной частотой
		PWM_Timer_Init(FirmWare_SRAM.freq);
		PWM_Timer_SetComp(0);
		PWM_Timer_Start();

		// Запускаем таймер захвата
		Duty_Timer_Init();
		Duty_Timer_Start();

		// Рассчитываем коэфициент впрыска от максимальной загрузки форсунки
		duty_coef = FirmWare_SRAM.max_duty / 48;

		// На всякий случай останавливаем насос
		PUMP_OFF();
	}
	else if (FirmWare_SRAM.type == 3) // Режим определения максимальной циклической загрузки
	{
		sprintf(str3, "Type: MAX DUTY ");
		disp_module_send_2_string(str3);
		disp_module_send_2_string(str3);
		LL_mDelay(1000);

		// Останавливаем таймер ШИМ
		PWM_Timer_Stop();

		// Запускаем таймер захвата
		Duty_Timer_Init();
		Duty_Timer_Start();

		// Сбрасываем максимальное значение загрузки
		FirmWare_SRAM.max_duty = 0;

		// На всякий случай останавливаем насос
		PUMP_OFF();
	}
	else
	{
		sprintf(str3, "Error prog!");
		disp_module_send_2_string(str3);
		LL_mDelay(1000);
	}

	WORK_Timer_Start();

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

		if (FUEL_STATE_CHECK(FirmWare_SRAM.fuel_state_type))
		{
			if (!LL_TIM_IsEnabledCounter(TIM1))
			{
				WORK_Timer_Start();
			}
			switch (FirmWare_SRAM.type) {
			case 1: // If type is MAP work
			{

				// отправка данных на дисплейный моудуль
				uint8_t percent = (uint8_t)((float)FirmWare_SRAM.inj_map[current_map] / 256 * 100);
				disp_module_sendMapData(dad_value, percent);

				LL_mDelay(100);

				break;
			}

			case 2: // If type is DUTY work
			{

				uint8_t percent = (uint8_t)((float)FirmWare_SRAM.inj_map2[current_map] / 256 * 100);
				uint8_t current_duty_in_percent = (uint8_t)((float)current_duty / 1024 * 100);

				disp_module_sendDutyData(current_duty_in_percent, RPM_value, percent);

				LL_mDelay(100);

				break;
			}

			case 3: // If type is finding max DUTY
			{
				// отправка данных на дисплейный моудуль
				uint8_t current_duty_in_percent = (uint8_t)((float)current_duty / 1024 * 100);
				uint8_t max_duty_in_percent = (uint8_t)((float)FirmWare_SRAM.max_duty / 1024 * 100);

				disp_module_sendMaxDutyData(current_duty_in_percent, max_duty_in_percent, RPM_value);

				LL_mDelay(100);
				break;
			}
			default:
				sprintf(str3, "Error program!!!");
				disp_module_send_2_string(str3);
				disp_module_send_1_string(str3);
				LL_mDelay(100);
				break;
			}

		}
		else
		{
			LL_mDelay(100);
			if (!FUEL_STATE_CHECK(FirmWare_SRAM.fuel_state_type))
			{
				WORK_Timer_Stop();

				PWM_Timer_SetComp(0);
				PUMP_OFF();

				// Отправка сигнала о конце топлива
				disp_module_sendFuelEnd();

				LL_mDelay(100);
			}
		}

		PC_UART_Poll();

	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);

	if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_2)
	{
		Error_Handler();
	}
	LL_RCC_HSE_Enable();

	/* Wait till HSE is ready */
	while(LL_RCC_HSE_IsReady() != 1)
	{

	}
	LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE_DIV_1, LL_RCC_PLL_MUL_7);
	LL_RCC_PLL_Enable();

	/* Wait till PLL is ready */
	while(LL_RCC_PLL_IsReady() != 1)
	{

	}
	LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
	LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
	LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
	LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

	/* Wait till System clock is ready */
	while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
	{

	}
	LL_Init1msTick(56000000);
	LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
	LL_SetSystemCoreClock(56000000);
	LL_RCC_SetADCClockSource(LL_RCC_ADC_CLKSRC_PCLK2_DIV_4);
}

/* USER CODE BEGIN 4 */
void WORK_1kHz_CallBack()
{
	LED_STOCK_TOG();
	switch (FirmWare_SRAM.type) {
	case 1: // If type is MAP work
	{
		current_map = adc_data / 85; // Приводим значение ацп к 48 при максимуме
		if (current_map >= 47) current_map = 47; // Определяем какую ячейку карты впрыска использовать

		if (FirmWare_SRAM.inj_map[current_map] > 0) // Если значение впрыска больше нуля то ключаем насос
		{
			PUMP_ON();
		}
		else
		{
			PUMP_OFF();
		}

		PWM_Timer_SetComp(FirmWare_SRAM.inj_map[current_map] * 4); // Устанавливаем степень впрыска

		dad_value = (((uint32_t)adc_data * FirmWare_SRAM.dad_type) / 4096);

		break;
	}

	case 2: // If type is DUTY work
	{
		current_duty = (uint16_t)((float)HighLevel_value / AllLevel_value * 1024);

		current_map = current_duty / duty_coef;

		if (current_map >= 47) current_map = 47;

		if (FirmWare_SRAM.inj_map2[current_map] > 0) PUMP_ON();
		else PUMP_OFF();

		PWM_Timer_SetComp(FirmWare_SRAM.inj_map2[current_map] * 4);

		RPM_value = 2000000 / AllLevel_value * 2;

		break;
	}

	case 3: // If type is finding max DUTY
	{
		current_duty = (uint16_t)((float)HighLevel_value / AllLevel_value * 1000);

		if (current_duty > FirmWare_SRAM.max_duty)
			FirmWare_SRAM.max_duty = current_duty;

		RPM_value = 2000000 / AllLevel_value * 2;

		break;
	}
	default:

		break;
	}
}


/*
 * Обработчик команд от ПК
 *
 * Поддерживаемые команды:
 * 1 - чтение прошивки
 * 2 - запись прошивки
 *
 * Чтение прошивки:
 * Команда - 1
 * Длинна - 1
 * Данные - 0х55
 * После приёма происходит отправка прошивки, структура пакета одинавкова, длинна равна размеру прошивки,
 * пакет содержит прошивку
 *
 */
void PC_UART_Poll(void)
{
	uint8_t tmp1 = 0xFF;
	uint8_t tmp2 = 0xFF;

	uint8_t packet_receive[256] = {0};

	uint8_t command = 0;
	uint8_t len = 0;
	uint8_t com_len_CRC = 0;
	uint8_t com_len_CRC_calc = 0;
	uint8_t pack_CRC = 0;

	while (RingBuffer_Size(&UART2_RX_RingBuffer)) // Цикл пока буфер не пуст
	{
		tmp1 = RingBuffer_Get(&UART2_RX_RingBuffer);

		if (tmp1 == 0x00) // Если пришёл IDLE
		{
			tmp2 = tmp1;
		}
		else if (tmp1 == 0xFF && tmp2 == 0x00) // Определяем приход START
		{
			// Готовимся к приёму нового пакета
			tmp2 = 0xFF; tmp1 = 0xFF;
			// Здесь обрабатываем приход пакета

			while (!RingBuffer_Size(&UART2_RX_RingBuffer));
			// Должна прийти команда
			command = RingBuffer_Get(&UART2_RX_RingBuffer);

			if (command != 0x01 || command != 0x02) return; // Если неправильная команда то выходим

			while (!RingBuffer_Size(&UART2_RX_RingBuffer));

			len = RingBuffer_Get(&UART2_RX_RingBuffer);

			while (!RingBuffer_Size(&UART2_RX_RingBuffer));

			com_len_CRC = RingBuffer_Get(&UART2_RX_RingBuffer);

			com_len_CRC_calc = command + len;
			com_len_CRC_calc = ~com_len_CRC_calc;
			com_len_CRC_calc += 1;

			if (com_len_CRC_calc != com_len_CRC) return;// Если CRC команды и пакета не правильный то выходим

			uint8_t sum = 0;
			for (uint8_t i = 0; i < len; i++) // Принимаем весь пакет
			{
				while (!RingBuffer_Size(&UART2_RX_RingBuffer));
				tmp1 = RingBuffer_Get(&UART2_RX_RingBuffer);

				packet_receive[i] = tmp1;
				sum += tmp1;
			}
			packet_receive[len] = 0;

			while (!RingBuffer_Size(&UART2_RX_RingBuffer));

			pack_CRC = RingBuffer_Get(&UART2_RX_RingBuffer);

			sum = ~sum;
			sum += 1;

			if (sum != pack_CRC) return; // Проверяем контрольную сумму пакета

			while (!RingBuffer_Size(&UART2_RX_RingBuffer));

			if (RingBuffer_Get(&UART2_RX_RingBuffer) != 0xFF) return;

			while (!RingBuffer_Size(&UART2_RX_RingBuffer));

			if (RingBuffer_Get(&UART2_RX_RingBuffer) != 0x00) return; // Не пришло условие стоп

			// Обработка принятых команд

			switch (command) {
			case 0x01: // Команда на чтение прошивки
				if (len != 1 || packet_receive[0] != 0x55) return; // Если пакет не правильный то выход

				// Начинаем отправку прошивки
				// Send start seq

				UART2_SendByte(START_CODE_1);
				UART2_SendByte(START_CODE_2);

				len = sizeof(FirmWare_SRAM);

				// Send command, len and CRC
				UART2_SendByte(0x01);
				UART2_SendByte(len);
				sum = 0;
				sum += 0x01;
				sum += len;
				sum = ~sum;
				sum += 1;
				UART2_SendByte(sum);

				// Send firmware
				uint8_t* FirmWare_ptr = (uint8_t*)(&FirmWare_SRAM);
				sum = 0;

				for (uint8_t i = 0; i < len; i++)
				{
					UART2_SendByte(*(FirmWare_ptr + i));
					sum += *(FirmWare_ptr + i);
				}

				// Send CRC of packet
				sum = ~sum;
				sum += 1;

				UART1_SendByte(sum);
				UART1_SendByte(START_CODE_2);
				UART1_SendByte(START_CODE_1);

				break;
			case 0x02: // Команда на запись прошивки
				WORK_Timer_Stop(); // Останавливаем работу системы

				sprintf(str3, "Programming...");
				disp_module_send_1_string(str3);
				LL_mDelay(10);
				disp_module_send_2_string("   ");
				LL_mDelay(100);
				disp_module_send_1_string(str3);
				LL_mDelay(100);


				// Устанавливаем текущую прошивку из принятого пакета
				FirmWare_SRAM = *(FirmWare*)(uint8_t*)packet_receive;
				// Записываем прошивку в оперативную память
				//FirmFare_WriteCurValue();

				LL_mDelay(500);
				sprintf(str3, "Write Complete!");
				disp_module_send_1_string(str3);
				LL_mDelay(10);
				disp_module_send_2_string("   ");
				LL_mDelay(100);

				WORK_Timer_Start(); // Возобновляем работу системы

				LL_mDelay(100);

				break;
			default:
				break;
			}

		}
	}
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while(1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{ 
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
