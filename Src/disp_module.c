/*
 * disp_module.c
 *
 *  Created on: 19 янв. 2019 г.
 *      Author: sergey
 */
#include "disp_module.h"

void disp_module_send_1_string(char * str)
{
	uint8_t sum = 0;
	uint8_t len = strlen(str);
	// Send start seq

	UART1_SendByte(START_CODE_1);
	UART1_SendByte(START_CODE_2);

	// Send command, len and CRC
	UART1_SendByte(0x01);
	UART1_SendByte(len);
	sum = 0;
	sum += 0x01;
	sum += len;
	sum = ~sum;
	sum += 1;
	UART1_SendByte(sum);
	// Send string
	sum = 0;
	for (uint8_t i = 0; i < len; i++)
	{
		UART1_SendByte(str[i]);
		sum += str[i];
	}
	// Send CRC of packet
	sum = ~sum;
	sum += 1;
	UART1_SendByte(sum);
	UART1_SendByte(START_CODE_2);
	UART1_SendByte(START_CODE_1);
}
void disp_module_send_2_string(char * str)
{
	uint8_t sum = 0;
	uint8_t len = strlen(str);
	// Send start seq
	UART1_SendByte(START_CODE_1);
	UART1_SendByte(START_CODE_2);
	// Send command, len and CRC
	UART1_SendByte(0x02);
	UART1_SendByte(len);
	sum = 0;
	sum += 0x02;
	sum += len;
	sum = ~sum;
	sum += 1;
	UART1_SendByte(sum);
	// Send string
	sum = 0;
	for (uint8_t i = 0; i < len; i++)
	{
		UART1_SendByte(str[i]);
		sum += str[i];
	}
	// Send CRC of packet
	sum = ~sum;
	sum += 1;
	UART1_SendByte(sum);
	UART1_SendByte(START_CODE_2);
	UART1_SendByte(START_CODE_1);
}

/*
 * Функция отправки данных при работе в режиме 1
 * Значения больше одного байта отправляются старшим байтом вперед
 * Код функции - 5
 * Содержимое пакета:
 * 1-2 байт - dad_value без смещения
 * 3 байт - степень впрыска в процентах
 * Длинна пакета равна 3
 */
void disp_module_sendMapData(uint16_t dad_value_in, uint8_t percent_in)
{
	uint8_t sum = 0;
	uint8_t len = 3;
	// Send start seq
	UART1_SendByte(START_CODE_1);
	UART1_SendByte(START_CODE_2);
	// Send command, len and CRC
	UART1_SendByte(0x05);
	UART1_SendByte(len);
	sum = 0;
	sum += 0x05;
	sum += len;
	sum = ~sum;
	sum += 1;
	UART1_SendByte(sum);
	// Send packet
	sum = 0;
	UART1_SendByte((uint8_t)(dad_value_in>>8));
	sum += (uint8_t)(dad_value_in>>8);

	UART1_SendByte((uint8_t)(dad_value_in));
	sum += (uint8_t)(dad_value_in);

	UART1_SendByte((uint8_t)(percent_in));
	sum += (uint8_t)(percent_in);
	// Send CRC of packet
	sum = ~sum;
	sum += 1;
	UART1_SendByte(sum);
	UART1_SendByte(START_CODE_2);
	UART1_SendByte(START_CODE_1);
}


/*
 * Функция отправки данных при работе в режиме 2
 * Значения больше одного байта отправляются старшим байтом вперед
 * Код функции - 6
 * Содержимое пакета:
 * 1 байт - циклическая загрузка форсунки в процентах
 * 2 байт - степень впрыска в процентах
 * 3-4 байты - обороты двигателя
 * Длинна пакета равна 4
 */
void disp_module_sendDutyData(uint8_t duty_in, uint16_t rpm_in, uint8_t percent_in)
{
	uint8_t sum = 0;
	uint8_t len = 4;
	// Send start seq
	UART1_SendByte(START_CODE_1);
	UART1_SendByte(START_CODE_2);
	// Send command, len and CRC
	UART1_SendByte(0x06);
	UART1_SendByte(len);
	sum = 0;
	sum += 0x06;
	sum += len;
	sum = ~sum;
	sum += 1;
	UART1_SendByte(sum);
	// Send packet
	sum = 0;
	UART1_SendByte((uint8_t)(duty_in));
	sum += (uint8_t)(duty_in);

	UART1_SendByte((uint8_t)(percent_in));
	sum += (uint8_t)(percent_in);

	UART1_SendByte((uint8_t)(rpm_in>>8));
	sum += (uint8_t)(rpm_in>>8);

	UART1_SendByte((uint8_t)(rpm_in));
	sum += (uint8_t)(rpm_in);
	// Send CRC of packet
	sum = ~sum;
	sum += 1;
	UART1_SendByte(sum);
	UART1_SendByte(START_CODE_2);
	UART1_SendByte(START_CODE_1);
}

/*
 * Функция отправки данных при работе в режиме 3
 * Значения больше одного байта отправляются старшим байтом вперед
 * Код функции - 7
 * Содержимое пакета:
 * 1 байт - циклическая загрузка форсунки в процентах
 * 2 байт - максимальная циклическая загрузка форсунки в процентах
 * 3-4 байты - обороты двигателя
 * Длинна пакета равна 4
 */
void disp_module_sendMaxDutyData(uint8_t duty_in, uint8_t max_duty_in, uint16_t rpm_in)
{
	uint8_t sum = 0;
	uint8_t len = 4;
	// Send start seq
	UART1_SendByte(START_CODE_1);
	UART1_SendByte(START_CODE_2);
	// Send command, len and CRC
	UART1_SendByte(0x07);
	UART1_SendByte(len);
	sum = 0;
	sum += 0x07;
	sum += len;
	sum = ~sum;
	sum += 1;
	UART1_SendByte(sum);
	// Send packet
	sum = 0;
	UART1_SendByte((uint8_t)(duty_in));
	sum += (uint8_t)(duty_in);

	UART1_SendByte((uint8_t)(max_duty_in));
	sum += (uint8_t)(max_duty_in);

	UART1_SendByte((uint8_t)(rpm_in>>8));
	sum += (uint8_t)(rpm_in>>8);

	UART1_SendByte((uint8_t)(rpm_in));
	sum += (uint8_t)(rpm_in);
	// Send CRC of packet
	sum = ~sum;
	sum += 1;
	UART1_SendByte(sum);
	UART1_SendByte(START_CODE_2);
	UART1_SendByte(START_CODE_1);
}

/*
 * Функция отправки уведомления о конце топлива
 * Значения больше одного байта отправляются старшим байтом вперед
 * Код функции - 8
 * Содержимое пакета:
 * 1 байт - 0x55
 * Длинна пакета равна 1
 */
void disp_module_sendFuelEnd(void)
{
	uint8_t sum = 0;
	uint8_t len = 1;
	// Send start seq
	UART1_SendByte(START_CODE_1);
	UART1_SendByte(START_CODE_2);
	// Send command, len and CRC
	UART1_SendByte(0x08);
	UART1_SendByte(len);
	sum = 0;
	sum += 0x08;
	sum += len;
	sum = ~sum;
	sum += 1;
	UART1_SendByte(sum);
	// Send packet
	UART1_SendByte((uint8_t)(0x55));
	// Send CRC of packet
	UART1_SendByte((uint8_t)~(uint8_t)0x55 + 1);
	UART1_SendByte(START_CODE_2);
	UART1_SendByte(START_CODE_1);
}
