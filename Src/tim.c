/**
  ******************************************************************************
  * File Name          : TIM.c
  * Description        : This file provides code for the configuration
  *                      of the TIM instances.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "tim.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* TIM1 init function */
void MX_TIM1_Init(void)
{
  LL_TIM_InitTypeDef TIM_InitStruct = {0};

  /* Peripheral clock enable */
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM1);

  /* TIM1 interrupt Init */
  NVIC_SetPriority(TIM1_UP_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
  NVIC_EnableIRQ(TIM1_UP_IRQn);

  TIM_InitStruct.Prescaler = 55;
  TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
  TIM_InitStruct.Autoreload = 1000;
  TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
  TIM_InitStruct.RepetitionCounter = 0;
  LL_TIM_Init(TIM1, &TIM_InitStruct);
  LL_TIM_EnableARRPreload(TIM1);
  LL_TIM_SetClockSource(TIM1, LL_TIM_CLOCKSOURCE_INTERNAL);
  LL_TIM_SetTriggerOutput(TIM1, LL_TIM_TRGO_RESET);
  LL_TIM_DisableMasterSlaveMode(TIM1);

}
/* TIM2 init function */
void MX_TIM2_Init(void)
{
  LL_TIM_InitTypeDef TIM_InitStruct = {0};
  LL_TIM_OC_InitTypeDef TIM_OC_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
  /* Peripheral clock enable */
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);

  TIM_InitStruct.Prescaler = 0;
  TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
  TIM_InitStruct.Autoreload = 1023;
  TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
  LL_TIM_Init(TIM2, &TIM_InitStruct);
  LL_TIM_DisableARRPreload(TIM2);
  LL_TIM_SetClockSource(TIM2, LL_TIM_CLOCKSOURCE_INTERNAL);
  LL_TIM_OC_EnablePreload(TIM2, LL_TIM_CHANNEL_CH1);
  TIM_OC_InitStruct.OCMode = LL_TIM_OCMODE_PWM1;
  TIM_OC_InitStruct.OCState = LL_TIM_OCSTATE_DISABLE;
  TIM_OC_InitStruct.OCNState = LL_TIM_OCSTATE_DISABLE;
  TIM_OC_InitStruct.CompareValue = 512;
  TIM_OC_InitStruct.OCPolarity = LL_TIM_OCPOLARITY_HIGH;
  LL_TIM_OC_Init(TIM2, LL_TIM_CHANNEL_CH1, &TIM_OC_InitStruct);
  LL_TIM_OC_DisableFast(TIM2, LL_TIM_CHANNEL_CH1);
  LL_TIM_SetTriggerOutput(TIM2, LL_TIM_TRGO_RESET);
  LL_TIM_DisableMasterSlaveMode(TIM2);
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);
    /**TIM2 GPIO Configuration    
    PA15     ------> TIM2_CH1 
    */
  GPIO_InitStruct.Pin = PWM_Pin;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
  LL_GPIO_Init(PWM_GPIO_Port, &GPIO_InitStruct);

  LL_GPIO_AF_RemapPartial1_TIM2();

}

/* USER CODE BEGIN 1 */
void WORK_Timer_Init(uint16_t freq_in)
{
	MX_TIM1_Init();
}
void WORK_Timer_Start(void)
{
	LL_TIM_EnableCounter(TIM1);
	LL_TIM_EnableIT_UPDATE(TIM1);
}
void WORK_Timer_Stop(void)
{
	LL_TIM_DisableCounter(TIM1);
	LL_TIM_DisableIT_UPDATE(TIM1);
}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

void PWM_Timer_Init(uint16_t freq_in)
{
	MX_TIM2_Init();
	LL_TIM_SetPrescaler(TIM2, (uint16_t)((uint32_t)56000000/(freq_in * 1023)));
	LL_TIM_CC_EnableChannel(TIM2, LL_TIM_CHANNEL_CH1);
}

void PWM_Timer_SetFreq(uint16_t freq_in)
{
	LL_TIM_SetPrescaler(TIM2, (uint16_t)((uint32_t)56000000/(freq_in * 1023)));
}

void PWM_Timer_SetComp(uint16_t duty)
{
	if (duty > 1023) duty = 1023;
	LL_TIM_OC_SetCompareCH1(TIM2, duty);
}

void PWM_Timer_Start(void)
{
	LL_TIM_EnableCounter(TIM2);
}

void PWM_Timer_Stop(void)
{
	LL_TIM_DisableCounter(TIM2);
}


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


void Duty_Timer_Init(void)
{
	// Init value
	TIM4_CCR1_value = 0;
	TIM4_CCR2_value = 0;
	TIM4_OVF1_value = 0;
	TIM4_OVF2_value = 0;
	HighLevel_value = 0;
	AllLevel_value = 0;

	// Init Port CH1
	// TIM4_CH1 <-> PB6 as Input Floating
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN; // Alternate Function Enable RCC
	// Init PB6 as Input Floating
	GPIOB->CRL &= ~GPIO_CRL_MODE6;
	GPIOB->CRL &= ~GPIO_CRL_CNF6;
	GPIOB->CRL |= GPIO_CRL_CNF6_0;
	// No Remap Alt Func


	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN; // Enable RCC TIM4

//	TIM4->PSC = 56 - 1; // 1 MHz
	TIM4->PSC = 28 - 1; // 2 MHz


	TIM4->DIER = TIM_DIER_CC1IE | TIM_DIER_UIE | TIM_DIER_CC2IE; // Overflow, CH1 and CH2 Interrupt Enable

	TIM4->CCMR1 = TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_1; // No Filter, no PSC, CH1 as input TI1

	TIM4->CCMR2 = 0; // Reset channel 3 and 4

	TIM4->CCER = TIM_CCER_CC1P |
			 TIM_CCER_CC1E | TIM_CCER_CC2E;; // IC1 Falling Edge

	TIM4->SMCR = TIM_SMCR_TS_2 | TIM_SMCR_TS_0 |
			TIM_SMCR_SMS_2; // Select trigger input

	TIM4->DCR = 0; // DMA register reset
	TIM4->DMAR = 0;

	TIM4->SR = 0; // All flags clear

	TIM4->CR2 = 0;
	TIM4->CR1 = TIM_CR1_URS;

	NVIC_EnableIRQ(TIM4_IRQn);
	NVIC_SetPriority(TIM4_IRQn, 0);
}
inline void Duty_Timer_Start(void)
{
	TIM4->CR1 |= TIM_CR1_CEN; // Counting Enable
}
inline void Duty_Timer_Stop(void)
{
	TIM4->CR1 &= ~TIM_CR1_CEN; // Counting Disable
}
inline void Duty_Timer_SetPSC(uint16_t PSC_value)
{
	TIM4->PSC = PSC_value;
	TIM4->EGR |=TIM_EGR_UG; // Generate Update
}

void TIM4_IRQHandler(void)
{
	if ((TIM4->SR & TIM_SR_CC1OF) || (TIM4->SR & TIM_SR_CC2OF))
	{
		TIM4->SR &= ~TIM_SR_CC1OF;
		TIM4->SR &= ~TIM_SR_CC2OF;
	}
	if (TIM4->SR & TIM_SR_CC1IF)
	{
		TIM4_CCR1_value = TIM4->CCR1;
		LED_STOCK_ON();
		HighLevel_value = (uint32_t)TIM4_OVF2_value * 65535 + TIM4_CCR2_value;
		AllLevel_value = (uint32_t)TIM4_OVF1_value * 65535 + TIM4_CCR1_value;
		TIM4_OVF1_value = 0;
		TIM4_OVF2_value = 0;
	}
	if (TIM4->SR & TIM_SR_CC2IF)
	{
		TIM4_CCR2_value = TIM4->CCR2;
		LED_STOCK_OFF();
		TIM4_OVF2_value = TIM4_OVF1_value;
	}
	if (TIM4->SR & TIM_SR_UIF)
	{
		TIM4->SR &= ~TIM_SR_UIF;
		TIM4_OVF1_value++;
	}
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
