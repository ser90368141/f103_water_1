/*
 * firmware.c
 *
 *  Created on: 17 мая 2018 г.
 *      Author: sergey
 *  Этот модуль определяет работу с прошивкой ЭБУ (файл конфигурации)
 */

#include "firmware.h"

// Функция загрузки прошивки в оперативную память
void FirmWare_Init(void)
{
	FirmWare_SRAM = *(FirmWare*)(void*)FLASH_ADR; // Загружаем прошивку в оперативную память
}

/*
 * В этой функции устанавливаются значения прошивки по умолчанию
 * Частота ШИМ 10 Гц
 * Максимальная циклическая загрузка форсунки максимальна
 * Тип ДАД - 2.5 Бар абсолютного давления
 * Тип работы системы - по ДАД
 * Тип работы датчика топлива - замыкание
 * Карты впрыска линейны от 0 до максимума
 */
void FirmWare_SetStockValue(void)
{
	FirmWare_SRAM.freq = 10;
	FirmWare_SRAM.max_duty = 1000;
	FirmWare_SRAM.dad_type = 250; //
	FirmWare_SRAM.type = 1;
	FirmWare_SRAM.fuel_state_type = 0;

	uint8_t q = 0;
	for (uint8_t i = 0; i < 48; i++)
	{
		FirmWare_SRAM.inj_map[i] = q;
		q += 5;
	}
	q = 0;
	for (uint8_t i = 0; i < 48; i++)
	{
		FirmWare_SRAM.inj_map2[i] = q;
		q += 5;
	}
}

/*
 * Функция записи текущей прошивки в память FLASH
 */
void FirmFare_WriteCurValue(void)
{
	uint32_t adr1 = FLASH_ADR;
	uint32_t *ptr = (uint32_t*)(&FirmWare_SRAM);
	uint32_t data = 0;

	// Calc length data in SRAM
	uint8_t data_len = (sizeof(FirmWare_SRAM) - 1) / 4; // number of word
	if ((sizeof(FirmWare_SRAM) - 1) % 4) data_len++;

	// Write SRAM firmware data in FLASH
	FLASH_Unlock();
	FLASH_Erase_Page(FLASH_ADR);
	for (uint8_t i = 0; i < data_len; i++)
	{
		adr1 = FLASH_ADR + (4 * i);
		data = *(ptr + i);
		FLASH_Program_Word(adr1, data);
	}
	FLASH_Lock();
}
