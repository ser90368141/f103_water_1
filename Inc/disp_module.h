/*
 * disp_module.h
 *
 *  Created on: 19 янв. 2019 г.
 *      Author: sergey
 */

#ifndef DISP_MODULE_H_
#define DISP_MODULE_H_

/*
 * Данный модуль отвечает за отправку данных на модуль индекации.
 *
 * Описаение протокола передачи:
 * 1 byte - 0x00 - first start byte
 * 2 byte - 0xFF - second start byte
 * 3 byte - command
 * 4 byte - pack len
 * 5 byte - command and len CRC
 * n byte - packet
 * n+1 byte - packet CRC
 * n+2 byte - 0xFF - first stop byte
 * n+3 byte - 0x00 - second stop byte
 *
 * Список команд:
 * 1 - вывод аски в первую строку
 * 2 - вывод аски во вторую строку
 * 5 - отправка данных при работе в режиме 1
 * 6 - отправка данных при работе в режиме 2
 * 7 - отправка данных при работе в режиме 3
 * 8 - отправка уведомления о конце топлива
 *
 * Контроллер должен отвечать на выполнение каждой команды
 * Структура ACK:
 * 1 byte - 0x00 - first start byte
 * 2 byte - 0xFF - second start byte
 * 3 byte - receive command (0xFF if have mistake)
 * 4 byte - 0xFF - first stop byte
 * 5 byte - 0x00 - second stop byte
 */
#include "string.h"
#include "usart.h"

#define START_CODE_1 	(0x00)
#define START_CODE_2 	(0xFF)

/*
 * Функции отправки аски в первую строку
 */
void disp_module_send_1_string(char * str);
void disp_module_send_2_string(char * str);

// Функция отправки данных при работе в типе 1
void disp_module_sendMapData(uint16_t dad_value_in, uint8_t percent_in);

// Функция отправки данных при работе в типе 2
void disp_module_sendDutyData(uint8_t duty_in, uint16_t rpm_in, uint8_t percent_in);

// Функция отправки данных при работе в типе 3
void disp_module_sendMaxDutyData(uint8_t duty_in, uint8_t max_duty_in, uint16_t rpm_in);

// Функция отправки уведомления о конце топлива
void disp_module_sendFuelEnd(void);

#endif /* DISP_MODULE_H_ */
