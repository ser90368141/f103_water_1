/*
 * uart.h
 *
 *  Created on: 9 мая 2018 г.
 *      Author: sergey
 */

#ifndef UART_MY_H_
#define UART_MY_H_

#include "main.h"

#define UART1_BusFreq 56000000
#define UART2_BusFreq 28000000

void UART1_Port_Init(void);
void UART2_Port_Init(void);

uint8_t UART1_Init(uint32_t baudrate_in);
uint8_t UART2_Init(uint32_t baudrate_in);

void UART1_TransmitEnable(void);
void UART2_TransmitEnable(void);

void UART1_TransmitDisable(void);
void UART2_TransmitDisable(void);

void UART1_ReciveEnable(void);
void UART2_ReciveEnable(void);

void UART1_ReciveDisable(void);
void UART2_ReciveDisable(void);

void UART1_SendByte(uint8_t byte_in);
void UART2_SendByte(uint8_t byte_in);

void UART1_SendBuf(uint8_t* Buf_in, uint16_t Buf_len_in);
void UART2_SendBuf(uint8_t* Buf_in, uint16_t Buf_len_in);

void UART1_SendDAD(int DAD_value);
void UART1_SendPercent(uint8_t pers_in);
void UART1_SendDutyRPM(uint16_t duty, uint16_t rpm_value);
void UART1_SendMaxDuty(uint16_t duty);
#endif /* UART_MY_H_ */
