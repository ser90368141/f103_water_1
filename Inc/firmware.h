/*
 * firmware.h
 *
 *  Created on: 17 мая 2018 г.
 *      Author: sergey
 */

#ifndef FIRMWARE_H_
#define FIRMWARE_H_

#include "flash.h"
#include "main.h"

typedef struct
{
	uint16_t freq; // Частота работы ШИМ
	uint16_t max_duty; // Максимальная загрузка бензиновой форсунки (0 - 1024)
	uint16_t dad_type; // Тип ДАД (250, 300, 350, 400)
	uint8_t type; // Тип работы системы
	uint8_t fuel_state_type; // Тип срабатывания датчика окончания топлива (0 - замыкание, 1 - размыкание)
	uint8_t inj_map[48]; // Карта впрыска по ДАД
	uint8_t inj_map2[48]; // Карта впрыска по Циклической загрузке.
} FirmWare;

FirmWare FirmWare_SRAM;

void FirmWare_Init(void);
void FirmWare_SetStockValue(void);
void FirmFare_WriteCurValue(void);

#endif /* FIRMWARE_H_ */
