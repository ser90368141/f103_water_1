/*
 * flash.h
 *
 *  Created on: 17 мая 2018 г.
 *      Author: sergey
 */

#ifndef FLASH_H_
#define FLASH_H_

#include "stdlib.h"
#include "main.h"

#define FLASH_ADR ((uint32_t)0x0800FC00)
#define FLASH_SIZE 1024
#define FLASH_END ((uint32_t)0x0800FFFF)

void FLASH_Unlock(void);
void FLASH_Lock(void);
uint8_t flash_ready(void);
uint8_t check_EOP(void);
uint8_t FLASH_Erase_Page(uint32_t address);
uint8_t FLASH_Program_Word(uint32_t address, uint32_t data);

#endif /* FLASH_H_ */
